import decimal
from re import match
from datetime import datetime
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.decorators import api_view


def index(request):
    fileHistory = open("apidata/history.txt","r")
    data = fileHistory.readlines()
    fileHistory.close()
    pulseHistory=[]
    for entry in data:
        pulseHistory.append(int(entry.split(" ")[0]))
    currentPulse = pulseHistory[-1]
    avg = round(sum(pulseHistory) / len(pulseHistory),2)
    maximum = max(pulseHistory)
    minimum = min(pulseHistory)
    warningValue = int(currentPulse) - int(avg)
    if "dateSearch" in request.GET:
        if request.GET.get('dateSearch') != "":
            fileredData = []
            date,time = request.GET.get('dateSearch','').split("T")
            date = date.replace("-","/")
            date = "/".join(date.split("/")[::-1])
            timeBound = str(int(time.split(":")[0]) + 1) + ":00"
            print (date,time)
            for entry in data:
                p,entryDate,entryTime = entry.split(" ")
                if (entryDate == date and entryTime >= time and entryTime < timeBound):
                    fileredData.append(entry)
            data = fileredData
    data.reverse()
    return render(request, 'index.html', {"data" : data, "currentPulse" : currentPulse, "avg" : avg, "max" : maximum, "min" : minimum, "warningValue" : warningValue})

@api_view(["GET"])
def datapoint(request):
    if ( "data" in request.query_params):
        if (match('^[1-9][0-9]{0,2}$',request.query_params['data'])):
            time = datetime.strftime(datetime.now(), '%d/%m/%Y %H:%M:%S')
            toHistoryString = request.query_params['data'] + " " + time + "\n"
            fileHistory = open("apidata/history.txt","a+")
            fileHistory.write(toHistoryString)
            fileHistory.close()
            return HttpResponse("OK!\n")
        else: 
            return HttpResponse("Bad request.\n")
    else:
        return HttpResponse("Bad request.\n")